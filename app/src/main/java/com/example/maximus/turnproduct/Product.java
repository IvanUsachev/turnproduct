package com.example.maximus.turnproduct;

public class Product {
    String productName;
    String productSum;

    public Product(String productName, String productSum) {
        this.productName = productName;
        this.productSum = productSum;
    }

    public String getProductName() {
        return productName;
    }

    public String getProductSum() {
        return productSum;
    }
}
