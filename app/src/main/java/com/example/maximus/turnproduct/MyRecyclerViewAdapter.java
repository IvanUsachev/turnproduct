package com.example.maximus.turnproduct;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private Context context;
    private List<Product> products;
    private OnRecycllItemClick onRecycllItemClick;


    public MyRecyclerViewAdapter(Context context, List<Product> products, OnRecycllItemClick onRecycllItemClick) {
        this.context = context;
        this.products = products;
        this.onRecycllItemClick = onRecycllItemClick;
    }

    public interface OnRecycllItemClick {
        void onClick(int position);
    }

    public void addItem(Product product) {
        products.add(1, product);
        notifyItemInserted(1);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.product_info, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = products.get(position);
        holder.productName.setText(product.getProductName());
        holder.productSum.setText(product.getProductSum());

//        int valuePlusDepost = Integer.parseInt(getValue(plusDeposit));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public Product getItem(int position) {
        return products.get(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView productName;
        TextView productSum;


        public ViewHolder(View item) {
            super(item);
            productName = (TextView) item.findViewById(R.id.productName);
            productSum = (TextView) item.findViewById(R.id.productSum);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRecycllItemClick.onClick(getAdapterPosition());
                }
            });
        }
    }
}



